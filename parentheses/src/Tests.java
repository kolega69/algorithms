import org.junit.Test;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;

import static org.junit.Assert.*;

public class Tests {

    private long[] katalanNums = {1, 1, 2, 5, 14, 42, 132, 429, 1430, 4862, 16796,
            58786, 208012, 742900, 2674440, 9694845, 35357670, 129644790, 477638700,
            1767263190, 6564120420l, 24466267020l, 91482563640l, 343059613650l,
            1289904147324l, 4861946401452l};
    private int[] testNums = {0, 2, 7, 12, 15};

    @Test
    public void correct_input() throws IOException{
        int numForInput = 2;
        setMessages(numForInput);
        QuantityOfParenthesesSequences qops = new QuantityOfParenthesesSequences();
        int read = qops.readFromConsole();
        assertEquals(read, numForInput);
    }

    @Test
    public void correct_computation() {
        QuantityOfParenthesesSequences qops = new QuantityOfParenthesesSequences();
        for (int testNum : testNums) {
            long result = qops.getQuantityOfSequences(testNum);
            //test num also matches to an index in Katalan numbers
            assertEquals(result, katalanNums[testNum]);
        }
    }

    public void setMessages(int number) {
        byte[] buf = String.valueOf(number).getBytes();
        InputStream is = new ByteArrayInputStream(buf);
        System.setIn(is);
    }
}
