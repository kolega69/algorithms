import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

/**
 * Let's imagine you have n pairs of parentheses and you need to form correct
 * sequences of them, where “valid” means that each open parenthesis has a
 * matching closed parenthesis. For instance, "(()(()))" is valid, but "()("
 * or "())(" is not. How many sequences are there for a given value of n
 * opening parentheses and n closed parentheses?
 * So this class resolves given above problem.
 */
public class QuantityOfParenthesesSequences {

    /**
     * In this method user will be asked for entering a positive integer
     * and then this number will be passed father for counting all possible
     * correct combination of the parentheses
     * @param args
     * @throws IOException
     */
    public static void main(String[] args) throws IOException {
        QuantityOfParenthesesSequences qps = new QuantityOfParenthesesSequences();
        int numOfParentheses = qps.readFromConsole();
        long sequencesQuantity = qps.getQuantityOfSequences(numOfParentheses);

        System.out.printf("Quantity of given parentheses pairs: %d \n" +
                          "Quantity of correct parentheses sequences: %d \n",
                          numOfParentheses, sequencesQuantity);
    }

    /**
     * Read data from console
     * @return int number
     * @throws IOException
     */
    int readFromConsole() throws IOException{
        BufferedReader bf = new BufferedReader(new InputStreamReader(System.in));
        int numOfParentheses;
        System.out.println("Enter positive integer please: ");
        while(true) {
            String number = bf.readLine();
            try {
                numOfParentheses = Integer.parseInt(number);
                break;
            } catch (NumberFormatException e) {
                System.out.println("Not an integer, try again");
            }
        }
        return numOfParentheses;
    }

    /**
     * Intermediate transitive method for algorithm simplification
     * @param numberParentheses - number of given parentheses pairs
     * @return - number of correct parentheses sequences
     */
    long getQuantityOfSequences(int numberParentheses) {
        if (numberParentheses <= 0) return 1;
        return getQuantityOfSequences(numberParentheses, numberParentheses);
    }

    /**
     * Method for counting number of correct parentheses sequences.
     * There are number of left or right parentheses is increased and then this method
     * have recursively invoking with new parameters and so on. When number of right
     * and left parentheses will be equal to zero, then sequences counter increases and
     * recursive task go back.
     * @param numLePar - number of remaining left parentheses
     * @param numRiPar - number of remaining right parentheses
     * @return - number of correct parentheses sequences
     */
    private long getQuantityOfSequences(int numLePar, int numRiPar) {
        //variable for counting correct combination of parentheses
        long seqCounter = 0;
        // Set left parentheses and decrease their remaining number
        if (numLePar > 0) {
            if (numLePar == 1 && numLePar == numRiPar) return 1;
            seqCounter += getQuantityOfSequences(numLePar - 1, numRiPar);
        }
        // Set right parentheses and decrease their remaining number
        if (numRiPar > 0 && numRiPar > numLePar) {
            if (numLePar == 0 && numRiPar == 1) return 1;
            seqCounter += getQuantityOfSequences(numLePar, numRiPar - 1);
        }
        return seqCounter;
    }
}
