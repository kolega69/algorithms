import org.junit.Test;
import static org.junit.Assert.*;

import java.math.BigInteger;

public class Tests {

    int[] testNums = {0, 2, 5, 10, 16, 19};
    BigInteger[] testResults = {
            new BigInteger("1"),
            new BigInteger("2"),
            new BigInteger("120"),
            new BigInteger("3628800"),
            new BigInteger("20922789888000"),
            new BigInteger("121645100408832000")
    };
    int[] digitsSum = {1, 2, 3, 27, 63, 45};


    @Test
    public void the_factorials_is_right() {

        assertEquals(testNums.length, testResults.length);
        for (int i = 0; i < testNums.length; i++) {

            BigInteger testNum = new BigInteger(String.valueOf(testNums[i]));
            BigInteger nF = SumOfDigits.computeFactorial(testNum);
            assertEquals(testResults[i], nF);
        }
    }

    @Test
    public void the_sum_is_right() {
        assertEquals(testResults.length, digitsSum.length);

        for (int i = 0; i < testResults.length; i++) {
            int sum = SumOfDigits.computeSumOfDigits(testResults[i]);
            assertEquals(sum, digitsSum[i]);
        }
    }
}
