import java.math.BigInteger;

/**
 * Find the sum of the digits in the number 100! (i.e. 100 factorial)
 * {Correct answer: 648}
 */
public class SumOfDigits {

    public static void main(String[] args) {
        // use BigInteger type for huge numbers
        BigInteger nF = computeFactorial(new BigInteger("100"));

        System.out.println(computeSumOfDigits(nF));
    }

    /**
     * compute sum of all the digits in the number
     * @param nF - number factorial
     * @return int
     */
    static int computeSumOfDigits(BigInteger nF) {
        int sumOfDigits = 0;
        // Divide by 10 and get digits
        while(true) {
            BigInteger[] divideRemainder = nF.divideAndRemainder(new BigInteger("10"));
            BigInteger intPart = divideRemainder[0];
            int remainder = divideRemainder[1].intValue();
            sumOfDigits += remainder;
            if (intPart.equals(new BigInteger("0"))) break;
            nF = intPart;
        }
        return sumOfDigits;
    }

    /**
     * Compute n!
     * @param n number to find it's factorial
     * @return factorial of number n
     */
    static BigInteger computeFactorial(BigInteger n) {
        if (n.compareTo(new BigInteger("1")) <= 0) return new BigInteger("1");
        BigInteger nextN = computeFactorial(n.subtract(new BigInteger("1")));
        return n.multiply(nextN);
    }
}
