import java.io.File;
import java.io.FileNotFoundException;
import java.util.*;

/**
 * You are given a list of cities. Each direct connection between
 * two cities has its transportation cost (an integer bigger than 0).
 * The goal is to find the paths of minimum cost between pairs of cities.
 * Assume that the cost of each path (which is the sum of costs of all
 * direct connections belonging to this path) is at most 200000.
 * The name of a city is a string containing characters a,...,z and
 * is at most 10 characters long.
 *
 * Input

 s [the number of tests <= 10]
 n [the number of cities <= 10000]
 NAME [city name]
 p [the number of neighbours of city NAME]
 nr cost [nr - index of a city connected to NAME (the index of the first city is 1)]
 [cost - the transportation cost]
 r [the number of paths to find <= 100]
 NAME1 NAME2 [NAME1 - source, NAME2 - end]
 [empty line separating the tests]

 Output

 cost [the minimum transportation cost from city NAME1 to city NAME2 (one per line)]
 */

public class MinimumCost {

    public static void main(String[] args) throws FileNotFoundException {

        File testData = new File("test_data.txt");
        Scanner scanner = new Scanner(testData);

        Map<Integer, City> cities = new HashMap<Integer, City>();
        Map<String, Integer> indexes = new HashMap<String, Integer>();

        int testsNum = scanner.nextInt();

        // repeat tests
        for (int num = 0; num < testsNum; num++) {
            int[][] sourceEndCities = readDataFromFile(scanner, indexes, cities);
            DijkstraAlgorithm diAlg = new DijkstraAlgorithm();
            List<Integer> results = diAlg.invokeDijkstra(sourceEndCities, cities);
            printResult(results);
        }

        scanner.close();
    }

    public static int[][] readDataFromFile(Scanner scanner, Map<String, Integer> indexes, Map<Integer, City> cities) {
        readCreateCities(scanner, cities, indexes);
        return readSetTestedPath(scanner, indexes);
    }

    /**
     * Method reads the cities number and then in loop reads name of the city and
     * indexes of it's neighbours with their transportation costs to them from
     * this one
     * @param scanner - the scanner instance for file reading
     * @param cities - the map of the cities to put in
     * @param indexes - the map of the indexes of the cities
     */
    private static void readCreateCities(Scanner scanner, Map<Integer, City> cities, Map<String, Integer> indexes) {
        int citiesNumber = scanner.nextInt();
        //read names of cities and their neighbours data
        for (int cn = 0; cn < citiesNumber; cn++) {
            String cityName = scanner.next();
            indexes.put(cityName, cn + 1);
            int cityNeighboursNum = scanner.nextInt();
            Map<Integer, Double> connections = new HashMap<Integer, Double>();

            for (int i = 0; i < cityNeighboursNum; i++) {
                int cityIndex = scanner.nextInt();
                double cost = scanner.nextDouble();
                connections.put(cityIndex, cost);
            }

            cities.put(cn + 1, new City(cn + 1, cityName, connections));
        }
    }

    /**
     * Method reads the number of the paths and then reads the names of the
     * source city and the destination city for each path.
     * @param scanner - the scanner instance for file reading
     * @return String[][] - the array of the paths
     */
    private static int[][] readSetTestedPath(Scanner scanner, Map<String, Integer> indexes) {
        int numberPaths = scanner.nextInt();
        int[][] sourceEndIndexes = new int[numberPaths][2];
        // take source and destination cities
        for (int i = 0; i < sourceEndIndexes.length; i++) {
            String source = scanner.next();
            String end = scanner.next();
            sourceEndIndexes[i][0] = indexes.get(source);
            sourceEndIndexes[i][1] = indexes.get(end);
        }
        return sourceEndIndexes;
    }

    /**
     * Print low costs to console
     * @param results
     */
    private static void printResult(List<Integer> results) {
        for (int res : results) {
            System.out.println(res);
        }
        System.out.println();
    }
}
