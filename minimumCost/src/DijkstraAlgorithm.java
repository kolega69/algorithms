import java.util.*;

/**
 * From Wikipedia:
 * "Dijkstra's algorithm, conceived by computer scientist Edsger Dijkstra in 1956
 * and published in 1959, is a graph search algorithm that solves the single-source
 * shortest path problem for a graph with non-negative edge path costs, producing a
 * shortest path tree. This algorithm is often used in routing and as a subroutine
 * in other graph algorithms."
 *
 * In this class I was going to invent "bicycle" for fun, but for real tasks there are
 * a lot of implementations of that algorithm have been suggested and implemented.
 */
public class DijkstraAlgorithm {

    /**
     * Method invokes the Dijkstra algorithm for the computation of the
     * cheapest route cost
     * @param sourEndIndex - the array of the paths
     * @param cities - map of the cities
     * @return cheapest paths
     */
    public List<Integer> invokeDijkstra(int[][] sourEndIndex, Map<Integer, City> cities) {
        List<Integer> results = new LinkedList<Integer>();

        for (int[] sourceEndCity : sourEndIndex) {
            int source = sourceEndCity[0];
            int dest = sourceEndCity[1];

            Map<Integer, City> tempCities = new HashMap<Integer, City>();
            //copy to a temporary list of cities because of clean method invocation needed
            for (Integer i : cities.keySet()) {
                City temp = cities.get(i).clone();
                tempCities.put(i, temp);
            }

            City startCity = tempCities.get(source);
            getAllPaths(tempCities, startCity);
            int cheapestPath = tempCities.get(dest).getCityCost().intValue();
            results.add(cheapestPath);

        }
        return results;
    }

    /**
     * Implementation of the Dijkstra Algorithm:
     * 1. All City class have it's own tentative cost assigned to infinity.
     * 2. Start point city cost will be assigned to 0;
     * 3. Then Neighbours of this city  and the connections between them are
     *    retrieved from current City class. The cost of start city are added
     *    to the cost of the connections to each neighbour and assigned to
     *    the connected cities if their cost bigger then new one.
     * 4. Within the new cost computation cycle the next start city is defined
     *    via comparing costs of the city. Next starting city has to have a
     *    lover cost.
     * 5. When all neighbors visited, then start city is blocked for computation.
     *    New city with a lower cost is assigned to be a start city and step 3 to 5
     *    repeats until all of the cities will be blocked.
     * 6. Now algorithm have computed all cheapest paths.
     * @param cities - the map of cities with their indexes
     * @param startCity - index of the city is assigned to be the start point
     */
    private void getAllPaths(Map<Integer, City> cities, City startCity) {

        City nextCityToVisit = null;

        // let's set weight of the first city to 0
        // so we get start point with an empty cost
        startCity.setCityCost(0d);

        while (true) {
            //retrieve all connections for the city
            //were we start or continue our traverse
            Map<Integer, Double> connections = startCity.getConnections();

            for (Integer cityIndex : connections.keySet()) {
                City neighbourCity = cities.get(cityIndex);
                //If particular city was visited we don't go there again
                if (neighbourCity.isBlocked()) continue;

                /*
                 * In the next steps we are comparing current cost of
                 * particular city and sum of weight of start city with cost
                 * of connection between this cities. If old weight is less,
                 * then it will be remained. Otherwise new weight will be
                 * assigned to the particular city
                 */
                Double conCost = connections.get(cityIndex);
                Double neighbourWeight = neighbourCity.getCityCost();
                Double probableNextWeight = conCost + startCity.getCityCost();

                double nextWeight = probableNextWeight < neighbourWeight ? probableNextWeight : neighbourWeight;
                neighbourCity.setCityCost(nextWeight);


                if (nextCityToVisit == null || neighbourWeight < nextCityToVisit.getCityCost()) {
                    nextCityToVisit = neighbourCity;
                }
            }

            // The main job done when all cities where visited
            if (nextCityToVisit == null) break;

            // Go to the next city with lower weight
            startCity.setBlocked(true);
            startCity = nextCityToVisit;
            nextCityToVisit = null;
        }
    }
}
