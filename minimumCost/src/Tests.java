import org.junit.Test;

import java.util.*;

import static org.junit.Assert.*;

public class Tests {

    private Map<Integer, City> cities = new HashMap<Integer, City>();
    private Map<String, Integer> indexes = new HashMap<String, Integer>();
    private int[][] paths = {{1, 4}, {2, 4}};
    private List<Integer> expectedNums = new ArrayList<Integer>();
    private String testInput = "4\n" +
                               "A\n" +
                               "2\n" +
                               "2 1\n" +
                               "3 3\n" +
                               "B\n" +
                               "3\n" +
                               "1 1\n" +
                               "3 1\n" +
                               "4 4\n" +
                               "C\n" +
                               "3\n" +
                               "1 3\n" +
                               "2 1\n" +
                               "4 1\n" +
                               "D\n" +
                               "2\n" +
                               "2 4\n" +
                               "3 1\n" +
                               "2\n" +
                               "A D\n" +
                               "B D";

    public void initCities() {
        Map<Integer, Double> connections1 = new HashMap<Integer, Double>();
        connections1.put(2, 1d);
        connections1.put(3, 3d);
        City a = new City(1, "A", connections1);

        Map<Integer, Double> connections2 = new HashMap<Integer, Double>();
        connections2.put(1, 1d);
        connections2.put(3, 1d);
        connections2.put(4, 4d);
        City b = new City(2, "B", connections2);

        Map<Integer, Double> connections3 = new HashMap<Integer, Double>();
        connections3.put(1, 3d);
        connections3.put(2, 1d);
        connections3.put(4, 1d);
        City c = new City(3, "C", connections3);

        Map<Integer, Double> connections4 = new HashMap<Integer, Double>();
        connections4.put(2, 4d);
        connections4.put(3, 1d);
        City d = new City(4, "D", connections4);

        cities.put(1, a);
        cities.put(2, b);
        cities.put(3, c);
        cities.put(4, d);

        expectedNums.add(3);
        expectedNums.add(2);
    }

    public void initIndexes() {
        indexes.put("A", 1);
        indexes.put("B", 2);
        indexes.put("C", 3);
        indexes.put("D", 4);
    }

    @Test
    public void invokeDijkstra_putData_getCost() {
        initCities();
        DijkstraAlgorithm diAl = new DijkstraAlgorithm();
        List<Integer> actualResults = diAl.invokeDijkstra(paths, cities);
        assertEquals(expectedNums, actualResults);
    }

    @Test
    public void readDataFromFile_putData_getArray() {
        initCities();
        initIndexes();
        Map<Integer, City> actualCities = new HashMap<Integer, City>();
        Map<String, Integer> actualIndexes = new HashMap<String, Integer>();
        Scanner scanner = new Scanner(testInput);
        int[][] actualPaths = MinimumCost.readDataFromFile(scanner, actualIndexes, actualCities);
        assertEquals(cities, actualCities);
        assertEquals(indexes, actualIndexes);
        assertArrayEquals(paths, actualPaths);
    }
}
