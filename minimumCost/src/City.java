import java.util.Map;

/**
 * This class represents the city (node).
 * At creation each city have an infinite cost.
 * Also it contains a map of all connections with
 * it's neighbours.
 */
public class City implements Cloneable{

    private int cityIndex;
    private Double cityCost;
    private String cityName;
    private boolean isBlocked = false;
    private Map<Integer, Double> connections;

    public City(int cityIndex, String cityName, Map<Integer, Double> connections) {
        this.cityIndex = cityIndex;
        this.cityName = cityName;
        this.connections = connections;
        cityCost = Double.POSITIVE_INFINITY;
    }

    /**
     * getter method for index of the city
     * @return int - the index
     */
    public int getCityIndex() {
        return cityIndex;
    }

    /**
     * getter method for the cost of the city
     * @return Double - the cost
     */
    public Double getCityCost() {
        return cityCost;
    }

    /**
     * getter method for the name of the city
     * @return name
     */
    public String getCityName() {
        return cityName;
    }

    /**
     * getter method for the status of the city
     * @return blocking status
     */
    public boolean isBlocked() {
        return isBlocked;
    }

    /**
     * getter method for the connections
     * @return map of the connections
     */
    public Map<Integer, Double> getConnections() {
        return connections;
    }

    /**
     * setter method for the city status
     * @param isBlocked true/false
     */
    public void setBlocked(boolean isBlocked) {
        this.isBlocked = isBlocked;
    }

    /**
     * setter method for the cost of the city
     * @param cityCost new cost
     */
    public void setCityCost(Double cityCost) {
        this.cityCost = cityCost;
    }

    /**
     * method for cloning instances of the City
     * @return new City object
     */
    public City clone() {
        return new City(cityIndex, cityName, connections);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof City)) return false;

        City city = (City) o;

        if (cityIndex != city.cityIndex) return false;
        if (isBlocked != city.isBlocked) return false;
        if (!cityCost.equals(city.cityCost)) return false;
        if (!cityName.equals(city.cityName)) return false;
        if (!connections.equals(city.connections)) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = cityIndex;
        result = 31 * result + cityCost.hashCode();
        result = 31 * result + cityName.hashCode();
        result = 31 * result + (isBlocked ? 1 : 0);
        result = 31 * result + connections.hashCode();
        return result;
    }
}
